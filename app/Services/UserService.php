<?php

namespace App\Services;

use App\DTO\AuthFormData;
use App\DTO\RegisterFormData;
use App\Exceptions\RegistrationErrorException;
use App\Models\Role;
use App\Models\User;
use App\src\DIContainer;
use App\src\Validation;
use App\src\ValidationInterface;

class UserService
{
    /**
     * New user registration
     * @throws RegistrationErrorException
     */
    public function register(RegisterFormData $form): bool
    {
        $role = Role::where('code', $form->getRoleCode())->first();

        if (is_null($role)) {
            return false;
        }

        $user = User::create([
            'name' => $form->getName(),
            'email' => $form->getEmail(),
            'password' => $form->getPassword()
        ]);

        $role->users()->save($user);

        return $user->exists();
    }

    /**
     * User exists?
     */
    public function exists(string $email): bool
    {
        return !is_null(User::where('email', $email)->first());
    }

    /**
     * To authenticate
     */
    public function auth(AuthFormData $form): bool
    {
        $validation = $this->validate($form);
        if (!$validation->isSuccess()) {
            return false;
        }

        $user = User::where('email', $form->getEmail())->first();

        if (is_null($user) || ($user->password !== $form->getPassword())) {
            return false;
        }

        return true;
    }

    /**
     * Set up validation rules and execute them
     */
    private function validate(AuthFormData $form): Validation
    {
        $validation = DIContainer::getInstance()->get(ValidationInterface::class);

        $validation->name('email')->value($form->getEmail())->rule('email')->required();
        $validation->name('password')->value($form->getPassword())->rule('password');

        return $validation;
    }
}