<?php

namespace App\Services;

use App\src\JWTConfig;
use DateTimeImmutable;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class JWTService
{
    private string $secretKey;
    private string $serverName;
    private string $alg;
    private Key $jwtKey;

    public function __construct(Key $jwtKey, JWTConfig $jwtConfig)
    {
        $this->secretKey = $jwtConfig->getSecretKey();
        $this->serverName = $jwtConfig->getServerName();
        $this->alg = $jwtConfig->getAlg();
        $this->jwtKey = $jwtKey;
    }

    /**
     * Generate a new token
     */
    public function generateToken(string $email): string
    {
        $issueAt = new DateTimeImmutable();

        $jwtData = [
            'iat' => $issueAt->getTimestamp(),
            'iss' => $this->serverName,
            'data' => [
                'email' => $email
            ]
        ];

        return JWT::encode($jwtData, $this->secretKey, $this->alg);
    }

    /**
     * Get a clean token from the header
     */
    public function getToken(string $header): string
    {
        preg_match('/Bearer\s(\S+)/', $header, $matches);
        return $matches[1] ?? '';
    }

    /**
     * This is a valid token?
     */
    public function isValidToken(string $jwtUserToken): void
    {
        JWT::decode($jwtUserToken, $this->jwtKey);
    }
}