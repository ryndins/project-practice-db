<?php

use App\src\Api;
use App\src\DIContainer;
use Slim\Factory\AppFactory;

require __DIR__ . '/bootstrap.php';

$routes = require __DIR__ . '/routes.php';
$definitions = require __DIR__ . '/di-config.php';

$container = DIContainer::getInstance();
$container->setDefinitions($definitions);

$slimApp = AppFactory::create();
$api = new Api($routes, $slimApp);
$api->run();