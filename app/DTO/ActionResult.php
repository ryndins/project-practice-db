<?php

namespace App\DTO;

class ActionResult
{
    private string $result;
    private int $statusCode;

    public function __construct(string $result, int $statusCode)
    {
        $this->result = $result;
        $this->statusCode = $statusCode;
    }

    /**
     * Get the result of the controller engine
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * Get http status code
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }
}