<?php

namespace App\DTO;

use Psr\Http\Message\ServerRequestInterface;

class RegisterFormData
{
    private string $name;
    private string $email;
    private string $password;
    private string $roleCode;

    public function __construct(ServerRequestInterface $request)
    {
        $parsedData = json_decode($request->getBody(), true);

        $this->name = $parsedData['name'];
        $this->email = $parsedData['email'];
        $this->password = $parsedData['password'];
        $this->roleCode = $parsedData['role'];
    }

    /**
     * Get the value of the name field
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get the value of the email field
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * Get the value of the password field
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * Get a role code
     */
    public function getRoleCode(): string
    {
        return $this->roleCode;
    }
}