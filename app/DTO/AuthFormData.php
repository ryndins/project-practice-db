<?php

namespace App\DTO;

use Psr\Http\Message\ServerRequestInterface;

class AuthFormData
{
    private string $email;
    private string $password;

    public function __construct(ServerRequestInterface $request)
    {
        $parsedData = json_decode($request->getBody(), true);

        $this->email = $parsedData['email'];
        $this->password = $parsedData['password'];
    }

    /**
     * @return mixed|string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed|string
     */
    public function getPassword()
    {
        return $this->password;
    }
}