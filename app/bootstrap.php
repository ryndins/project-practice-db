<?php

require __DIR__ . '/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();
$capsule->addConnection([
    'driver' => 'pgsql',
    'host' => 'postgresql',
    'database' => 'web_project',
    'username' => 'postgres',
    'password' => 'secret'
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();