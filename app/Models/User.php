<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, mixed|string $getEmail)
 * @method static create(array $array)
 */
class User extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name', 'email', 'password'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles');
    }
}