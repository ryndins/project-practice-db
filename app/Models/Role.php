<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static where(string $string, string $getRoleCode)
 */
class Role extends Model
{
    protected $fillable = [
        'name', 'code'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_roles');
    }
}
