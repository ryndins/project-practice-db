<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateRolesTable extends AbstractMigration
{
    public function change(): void
    {
        $roles = $this->table('roles');
        $roles
            ->addColumn('name', 'string')
            ->addColumn('code', 'string')
            ->create();
    }
}
