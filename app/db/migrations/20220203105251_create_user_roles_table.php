<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

final class CreateUserRolesTable extends AbstractMigration
{
    public function change(): void
    {
        $user_roles = $this->table('user_roles');
        $user_roles
            ->addColumn('user_id', 'integer')
            ->addColumn('role_id', 'integer')
            ->addForeignKey(
                'user_id', 'users', 'id',
                ['delete' => 'CASCADE', 'update' => 'NO_ACTION']
            )
            ->addForeignKey(
                'role_id', 'roles', 'id',
                ['delete' => 'CASCADE', 'update' => 'NO_ACTION']
            )
            ->create();
    }
}
