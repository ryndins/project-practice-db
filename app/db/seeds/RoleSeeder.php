<?php


use Phinx\Seed\AbstractSeed;

class RoleSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * https://book.cakephp.org/phinx/0/en/seeding.html
     */
    public function run()
    {
        $roles = $this->table('roles');

        $data = [
            [
                'name' => 'Администратор',
                'code' => 'Administrator'
            ],
            [
                'name' => 'Менеджер',
                'code' => 'Manager',
            ],
            [
                'name' => 'Продавец',
                'code' => 'Seller'
            ],
        ];

        $roles->insert($data)->saveData();
    }
}
