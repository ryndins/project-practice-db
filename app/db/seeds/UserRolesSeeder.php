<?php

use Illuminate\Support\Arr;
use Phinx\Seed\AbstractSeed;

class UserRolesSeeder extends AbstractSeed
{

    public function getDependencies(): array
    {
        return [
            'UserSeeder',
            'RoleSeeder'
        ];
    }

    public function run()
    {
        $user_roles = $this->table('user_roles');

        $data = [];

        $users = $this->adapter->fetchAll("SELECT id FROM Users");
        $roles = $this->adapter->fetchAll("SELECT id FROM Roles");

        foreach ($users as $user) {
            $data[] = [
                'user_id' => $user['id'],
                'role_id' => Arr::random(array_column($roles, 'id'))
            ];
        }

        $user_roles->insert($data)->saveData();
    }
}
