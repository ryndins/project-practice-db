<?php

namespace App\Middlewares;

use App\Services\JWTServiceInterface;
use App\src\DIContainer;
use App\src\HttpStatuses;
use Exception;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Psr7\Response;

class VerifyJWT
{
    private const JWT_NOT_VALID_MSG = 'Your JWT is not valid.';

    /**
     * @throws Exception
     */
    public function __invoke(Request $request, RequestHandler $handler): ResponseInterface
    {
        $response = new Response();

        if (!$request->hasHeader('Authorization')) {
            $response->getBody()->write(self::JWT_NOT_VALID_MSG);
            return $response->withStatus(HttpStatuses::UNAUTHORIZED);
        }

        try {
            $jwtService = DIContainer::getInstance()->get(JWTServiceInterface::class);
            $jwtUserToken = $jwtService->getToken($request->getHeaderLine('Authorization'));
            $jwtService->isValidToken($jwtUserToken);
        } catch (Exception $exception) {
            $response->getBody()->write(self::JWT_NOT_VALID_MSG);
            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(HttpStatuses::UNAUTHORIZED);
        }

        return $handler->handle($request);
    }
}