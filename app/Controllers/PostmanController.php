<?php

namespace App\Controllers;

use App\Exceptions\GuzzleClientException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Request;

class PostmanController
{
    /**
     * @throws GuzzleClientException
     */
    public function index(): string
    {
        $client = new Client(['base_uri' => getenv('POSTMAN_ECHO_URL')]);
        $headers = [
            'Accept' => 'application/json',
            'Authorization' => getenv('BASIC_AUTH_TOKEN')
        ];

        $request = new Request('GET', '/basic-auth', $headers);

        try {
            $response = $client->send($request);

            return $response->getBody()->getContents();

        } catch (GuzzleException $guzzleException) {
            throw new GuzzleClientException($guzzleException->getMessage(), $guzzleException->getCode());
        }
    }
}