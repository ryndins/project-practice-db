<?php

namespace App\Controllers;

use Psr\Http\Message\ServerRequestInterface;
use Slim\Factory\ServerRequestCreatorFactory;

class BaseController
{
    /**
     * Get the current request
     */
    protected function getRequest(): ServerRequestInterface
    {
        return ServerRequestCreatorFactory::create()->createServerRequestFromGlobals();
    }

    /**
     * Collect and send response
     */
    protected function makeResponseBody(bool $success, string $message, array $errors = []): array
    {
        $response['success'] = $success;
        $response['message'] = $message;

        if (!empty($errors)) {
            $response['errors'] = $errors;
        }

        return $response;
    }
}