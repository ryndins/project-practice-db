<?php

namespace App\Controllers;

use App\DTO\RegisterFormData;
use App\Exceptions\NotValidUserDataException;
use App\Exceptions\RegistrationErrorException;
use App\Services\UserService;
use App\Services\UserServiceInterface;
use App\src\DIContainer;
use App\src\Validation;
use App\src\ValidationInterface;
use Exception;

class RegisterController extends BaseController
{
    /**
     * @throws NotValidUserDataException
     * @throws Exception
     * @throws RegistrationErrorException
     */
    public function index(): array
    {
        $form = $this->getRegisterForm();
        $validation = $this->validate($form);

        if (!$validation->isSuccess()) {
            throw new NotValidUserDataException('Data has not been validated');
        }

        /** @var UserService $user */
        $user = DIContainer::getInstance()->get(UserServiceInterface::class);

        if ($user->exists($form->getEmail())) {
            throw new NotValidUserDataException('A user already exists.');
        }

        if (!$user->register($form)) {
            throw new RegistrationErrorException('User registration error. The role cannot be empty!');
        }

        return $this->makeResponseBody(true, 'User is successfully registered.');
    }

    /**
     * Set up validation rules and execute them
     * @throws Exception
     */
    private function validate(RegisterFormData $form): Validation
    {
        /** @var Validation $validation */
        $validation = DIContainer::getInstance()->get(ValidationInterface::class);

        $validation->name('name')->value($form->getName())->required();
        $validation->name('email')->value($form->getEmail())->rule('email')->required();
        $validation->name('password')->value($form->getPassword())->rule('password');

        return $validation;
    }

    /**
     * Get data from a form
     */
    private function getRegisterForm(): RegisterFormData
    {
        return new RegisterFormData($this->getRequest());
    }
}