<?php

namespace App\Controllers;

use App\DTO\AuthFormData;
use App\Exceptions\NotValidUserDataException;
use App\Services\JWTService;
use App\Services\JWTServiceInterface;
use App\Services\UserServiceInterface;
use App\src\DIContainer;
use Exception;

class AuthController extends BaseController
{

    public function index(): array
    {
        return [
            'success' => true,
        ];
    }

    public function logout(): array
    {
        return [
            'success' => true,
        ];
    }

    /**
     * Authenticate and get a JWT token
     * @throws NotValidUserDataException
     * @throws Exception
     */
    public function auth(): array
    {
        $form = $this->getAuthForm();
        $user = DIContainer::getInstance()->get(UserServiceInterface::class);

        if (!$user->auth($form)) {
            throw new NotValidUserDataException("Unauthorized user.");
        }

        /** @var JWTService $jwtService */
        $jwtService = DIContainer::getInstance()->get(JWTServiceInterface::class);
        $userToken = $jwtService->generateToken($form->getEmail());

        return $this->makeResponseBody(true, 'Your token: ' . $userToken);
    }

    /**
     * Get data from a form
     */
    private function getAuthForm(): AuthFormData
    {
        return new AuthFormData($this->getRequest());
    }
}
