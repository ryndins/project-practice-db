<?php

use App\Components\Mailer;
use App\Components\UserManager;
use App\Components\UserManagerInterface;
use App\Services\JWTKeyInterface;
use App\Services\JWTService;
use App\Services\JWTServiceInterface;
use App\Services\UserService;
use App\Services\UserServiceInterface;
use App\src\JWTConfig;
use App\src\Validation;
use App\src\ValidationInterface;
use Firebase\JWT\Key;
use function DI\create;
use function DI\get;

return [
    Mailer::class => create(Mailer::class),
    UserServiceInterface::class => create(UserService::class),
    ValidationInterface::class => create(Validation::class),
    UserManagerInterface::class => create(UserManager::class)
        ->constructor(get(Mailer::class)),
    JWTServiceInterface::class => create(JWTService::class)
        ->constructor(
            get(Key::class),
            get(JWTConfig::class)
        ),
    JWTConfig::class => create(JWTConfig::class)
        ->constructor(
            DI\env('JWT_SECRET_KEY'),
            DI\env('SERVER_NAME'),
            DI\env('JWT_ALGORITHM')
        ),
    Key::class => create(Key::class)
        ->constructor(
            DI\env('JWT_SECRET_KEY'),
            DI\env('JWT_ALGORITHM')
        )
];