<?php

namespace App\Components;

interface UserManagerInterface
{
    public function register(string $email, string $password);
}