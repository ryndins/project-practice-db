<?php

namespace App\Components;

use Exception;
use ReflectionClass;
use ReflectionException;

class MyDIContainer
{
    private array $classNameMap;
    private array $instances = [];

    public function __construct(array $classNameMap)
    {
        $this->classNameMap = $classNameMap;
    }

    /**
     * Add an instance to the container
     */
    private function set(string $className, object $instance): void
    {
        $this->instances[$className] = $instance;
    }

    /**
     * Get object from container
     * @throws ReflectionException
     */
    public function get(string $abstract): object
    {
        if (array_key_exists($abstract, $this->instances)) {
            return $this->instances[$abstract];
        }

        if (!array_key_exists($abstract, $this->classNameMap)) {
            $this->classNameMap[$abstract] = $abstract;
        }

        return $this->resolve($this->classNameMap[$abstract]);
    }

    /**
     * Resolve dependencies
     * @throws ReflectionException
     * @throws Exception
     */
    public function resolve(string $className): object
    {
        $reflector = new ReflectionClass($className);
        if (!$reflector->isInstantiable()) {
            throw new Exception("Class $className is not instantiable.");
        }

        $constructor = $reflector->getConstructor();
        if (is_null($constructor)) {
            $instance = $reflector->newInstance();
            $this->set($className, $instance);

            return $instance;
        }

        $parameters = $constructor->getParameters();
        $dependencies = $this->getDependencies($parameters);

        $instanceArgs = $reflector->newInstanceArgs($dependencies);
        $this->set($className, $instanceArgs);

        return $instanceArgs;
    }

    /**
     * Get dependencies from parameters
     * @throws ReflectionException
     * @throws Exception
     */
    private function getDependencies(array $parameters): array
    {
        $dependencies = [];

        foreach ($parameters as $parameter) {
            $dependency = $parameter->getType();

            if (!$dependency->isBuiltin()) {
                $dependencies[] = $this->get($dependency);
                continue;
            }

            if ($parameter->isDefaultValueAvailable()) {
                $dependencies[] = $parameter->getDefaultValue();
                continue;
            }

            throw new Exception("Can not resolve class dependency $parameter->name");
        }
        return $dependencies;
    }
}