<?php

namespace App\Components\GeoLocation;

interface GeoService
{
    public function getCoordinates(string $address): string;
}