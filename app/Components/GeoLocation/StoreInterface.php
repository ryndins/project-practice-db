<?php

namespace App\Components\GeoLocation;

interface StoreInterface
{
    public function getAddress(): string;
}