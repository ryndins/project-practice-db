<?php

namespace App\Components\GeoLocation;

class YandexMaps implements GeoService
{
    public function getCoordinates(string $address): string
    {
        return 'yandex.coordinates';
    }
}
