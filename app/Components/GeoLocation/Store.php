<?php

namespace App\Components\GeoLocation;

class Store implements StoreInterface
{
    public function getAddress(): string
    {
        return 'get.address.from.store';
    }

}