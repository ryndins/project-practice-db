<?php

namespace App\src;

class Request
{
    private string $uri;
    private string $method;
    private array $params;

    public function __construct(string $uri, string $method, array $params)
    {
        $this->uri = $this->getRequestUri($uri);
        $this->method = $method;
        $this->params = $params;
    }

    public function getUri(): string
    {
        return $this->uri;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * Get URI
     */
    private function getRequestUri(string $uri): string
    {
        return parse_url($uri)['path'];
    }
}