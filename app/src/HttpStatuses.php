<?php

namespace App\src;

class HttpStatuses
{
    const OK = 200;
    const BAD_REQUEST = 400;
    const UNAUTHORIZED = 401;
    const FORBIDDEN = 403;
    const INTERNAL_SERVER_ERROR = 500;
}