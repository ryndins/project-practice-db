<?php

namespace App\src;

use App\Exceptions\InternalServerErrorException;
use ArgumentCountError;
use ErrorException;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;

class App
{
    /**
     * Entry point to the application
     * @throws ErrorException
     */
    public function run(RequestData $requestData): void
    {
        $controller = $this->makeController($requestData->getControllerName());

        try {
            $reflectorClass = new ReflectionClass($controller);

            if (!$reflectorClass->hasMethod($requestData->getActionName())) {
                throw new InternalServerErrorException(HttpMessages::INTERNAL_SERVER_ERROR);
            }

            $reflectorMethod = new ReflectionMethod($controller, $requestData->getActionName());
            $response = $reflectorMethod->invokeArgs($controller, $requestData->getParams());
            Response::getInstance()->sendJSON($response);
        } catch (InternalServerErrorException|ReflectionException|ArgumentCountError $e) {
            throw new ErrorException($e->getMessage());
        }
    }

    /**
     * Get controller object
     */
    private function makeController(string $controllerName): object
    {
        return new $controllerName();
    }


}