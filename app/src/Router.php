<?php

namespace App\src;

use App\Exceptions\BadRequestException;

class Router
{
    private array $routes;
    private Request $request;

    private const CONTROLLERS_PATH = 'App\Controllers\\';

    public function __construct(array $routes, Request $request)
    {
        $this->routes = $routes;
        $this->request = $request;
    }

    /**
     * Collect all the ingredients
     * @throws BadRequestException
     */
    public function getRequestData(): RequestData
    {
        if (!$this->uriExists() || !$this->methodExists()) {
            throw new BadRequestException(HttpStatuses::BAD_REQUEST);
        }

        [$controllerName, $actionName] = $this->getRouteAction();
        $params = $this->getRequestParams();

        return new RequestData($controllerName, $actionName, $params);
    }

    /**
     * Get query parameters
     */
    private function getRequestParams(): array
    {
        return $this->request->getParams();
    }

    /**
     * Check the route
     */
    private function uriExists(): bool
    {
        return in_array($this->request->getUri(), array_keys($this->routes));
    }

    /**
     * Check the HTTP request method
     */
    private function methodExists(): bool
    {
        return in_array($this->request->getMethod(),
            $this->routes[$this->request->getUri()]['method']);
    }

    /**
     * Get full name class
     */
    private function getFullControllerName(string $controllerName): string
    {
        return self::CONTROLLERS_PATH . $controllerName;
    }

    /**
     * Get data from the Routes map
     */
    private function getRouteAction(): array
    {
        return explode('@', $this->routes[$this->request->getUri()]['action']);
    }
}