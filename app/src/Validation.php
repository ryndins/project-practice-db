<?php

namespace App\src;

class Validation
{
    private string $name;
    private string $value;
    private string $pattern;

    private array $errors = [];

    private string $defaultPassPattern = '[A-Za-z0-9-.;\'`_!#@]{5,15}';

    private const RULE_EXCEPTION_MESSAGE = 'There is no method for processing a rule.';
    private const ERROR_DEFAULT_MESSAGE = 'Please fill in the field correctly.';

    /**
     * Set field name
     */
    public function name(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Set value of the field
     */
    public function value(string $value): self
    {
        $this->value = $value;
        return $this;
    }

    public function rule(string $rule): self
    {
        if (!method_exists($this, $rule)) {
            $this->addError();
            return $this;
        }

        if (!$this->{$rule}()) {
            $this->addError();
            return $this;
        }

        return $this;
    }

    /**
     * Add check pattern
     */
    public function pattern(string $pattern): self
    {
        $this->pattern = $pattern;
        return $this;
    }

    /**
     * Check for a minimum number of characters
     */
    public function min(int $value): self
    {
        if (mb_strlen($this->value) < $value) {
            $this->addError();
        }

        return $this;
    }

    /**
     * Check for a maximum number of characters
     */
    public function max(int $value): self
    {
        if (mb_strlen($this->value) > $value) {
            $this->addError();
        }

        return $this;
    }

    /**
     * Checking the field for emptiness
     */
    public function required(): self
    {
        if (empty($this->value)) {
            $this->addError();
        }

        return $this;
    }

    /**
     * Validation was successful
     */
    public function isSuccess(): bool
    {
        return empty($this->getErrors());
    }


    /**
     * Check the field on the email
     */
    public function email(): bool
    {
        return filter_var($this->value, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Check the password by pattern
     */
    public function password(): bool
    {
        $pattern = (empty($this->pattern)) ? $this->defaultPassPattern : $this->pattern;
        return preg_match('/^('.$pattern.')$/u', $this->value);
    }

    /**
     * Get a list of errors
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * Add an error to the list
     */
    private function addError(): void
    {
        $this->errors[$this->name][] = self::ERROR_DEFAULT_MESSAGE;
    }
}