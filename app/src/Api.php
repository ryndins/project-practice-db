<?php

namespace App\src;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteInterface;

class Api
{
    private App $app;
    private const AVAILABLE_METHODS = [
        'GET', 'POST', 'OPTIONS', 'HEAD', 'PUT', 'PATCH', 'DELETE', 'TRACE',
    ];

    public function __construct(array $routes, App $app)
    {
        $this->app = $app;
        $this->makeMapRoutes($routes);
    }

    /**
     * Run the app
     */
    public function run(): void
    {
        $this->app->addErrorMiddleware(false, false, false);
        $this->app->run();
    }

    /**
     * Make a map of routes
     */
    private function makeMapRoutes(array $routes)
    {
        foreach ($routes as $route => $config) {
            [$controllerName, $actionName] = explode('@', $config['action']);
            $handler = $this->makeRouteHandler($controllerName, $actionName);

            foreach ($config['method'] as $method) {
                if (!in_array($method, self::AVAILABLE_METHODS)) {
                    continue;
                }

                $registeredRoute = $this->app->{strtolower($method)}($route, $handler);

                if (!empty($config['middleware'])) {
                    $this->addMiddlewares($registeredRoute, $config['middleware']);
                }

            }
        }
    }

    /**
     * Get route handler
     */
    private function makeRouteHandler(string $controllerName, string $actionName): callable
    {
        return function (Request $request, Response $response, array $args = [])
        use ($controllerName, $actionName) {

            $controllerEngine = new ControllerEngine($controllerName, $actionName, $args);
            $actionResult = $controllerEngine->execute();
            $response->getBody()->write($actionResult->getResult());

            return $response
                ->withHeader('Content-Type', 'application/json')
                ->withStatus($actionResult->getStatusCode());
        };
    }

    /**
     * Add middleware for route
     */
    private function addMiddlewares(RouteInterface $registeredRoute, array $middlewares): void
    {
        foreach ($middlewares as $middleware) {
            $registeredRoute->add(new $middleware());
        }
    }
}