<?php

namespace App\src;

class JWTConfig
{
    private string $secretKey;
    private string $serverName;
    private string $alg;

    /**
     * @param string $secretKey
     * @param string $serverName
     * @param string $alg
     */
    public function __construct(string $secretKey, string $serverName, string $alg)
    {
        $this->secretKey = $secretKey;
        $this->serverName = $serverName;
        $this->alg = $alg;
    }

    /**
     * Get secret key
     */
    public function getSecretKey(): string
    {
        return $this->secretKey;
    }

    /**
     * Get server name
     */
    public function getServerName(): string
    {
        return $this->serverName;
    }

    /**
     * Get encryption algorithm
     */
    public function getAlg(): string
    {
        return $this->alg;
    }
}