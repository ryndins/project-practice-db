<?php

namespace App\src;

use App\DTO\ActionResult;
use App\Exceptions\GuzzleClientException;
use App\Exceptions\InternalServerErrorException;
use App\Exceptions\RegistrationErrorException;
use App\Exceptions\NotValidUserDataException;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;

class ControllerEngine
{
    private string $controllerName;
    private string $actionName;
    private array $args;

    public function __construct(string $controllerName, string $actionName, array $args)
    {
        $this->controllerName = $controllerName;
        $this->actionName = $actionName;
        $this->args = $args;
    }

    /**
     * Execute controller action
     */
    public function execute(): ActionResult
    {
        try {
            $controller = new $this->controllerName();
            $controllerClass = new ReflectionClass($controller);

            if (!$controllerClass->hasMethod($this->actionName)) {
                throw new InternalServerErrorException(HttpMessages::INTERNAL_SERVER_ERROR);
            }

            $controllerMethod = new ReflectionMethod($controller, $this->actionName);
            $actionResult = $controllerMethod->invokeArgs($controller, $this->args);
            return $this->createActionResult(json_encode($actionResult), HttpStatuses::OK);

        } catch (ReflectionException|InternalServerErrorException|RegistrationErrorException $exception) {
            return $this->createActionResult($exception->getMessage(), HttpStatuses::INTERNAL_SERVER_ERROR);
        } catch (GuzzleClientException $guzzleClientException) {
            return $this->createActionResult($guzzleClientException->getMessage(), HttpStatuses::UNAUTHORIZED);
        } catch (NotValidUserDataException $exception) {
            return $this->createActionResult($exception->getMessage(), HttpStatuses::UNAUTHORIZED);
        }
    }

    /**
     * Create and return the result object of the controller with http status
     */
    private function createActionResult(string $result, int $status): ActionResult
    {
        return new ActionResult($result, $status);
    }
}