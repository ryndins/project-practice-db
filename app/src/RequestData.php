<?php

namespace App\src;

class RequestData
{
    private string $controllerName;
    private string $actionName;
    private array $params;

    public function __construct(string $controllerName, string $actionName, array $params)
    {
        $this->controllerName = $controllerName;
        $this->actionName = $actionName;
        $this->params = $params;
    }

    public function getControllerName(): string
    {
        return $this->controllerName;
    }

    public function getActionName(): string
    {
        return $this->actionName;
    }

    public function getParams(): array
    {
        return $this->params;
    }

}