<?php

namespace App\src;

class HttpMessages
{
    const BAD_REQUEST = '400 Bad Request';
    const INTERNAL_SERVER_ERROR = '500 Internal Server Error';
}