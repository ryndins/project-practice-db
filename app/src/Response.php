<?php

namespace App\src;

class Response
{
    private static $_instance;

    private function __construct()
    {
    }

    /**
     * Return an instance of the current class
     */
    public static function getInstance(): self
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Send http status to the client
     */
    public function sendStatus(int $code): void
    {
        http_response_code($code);
    }

    /**
     * Convert to JSON
     */
    public function sendJSON(array $array): void
    {
        echo json_encode($array);
    }
}