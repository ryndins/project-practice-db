<?php

namespace App\src;

use DI\Container;
use DI\ContainerBuilder;
use DI\DependencyException;
use DI\NotFoundException;
use Exception;

class DIContainer
{
    private static self $instance;
    private array $definitions;

    /**
     * Get an instance of the container
     */
    public static function getInstance(): self
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Get the requested instance
     * @throws Exception
     */
    public function get(string $interface)
    {
        try {
            $container = $this->makeContainer();
            return $container->get($interface);
        } catch (DependencyException|NotFoundException|Exception $exception) {
            die($exception->getMessage());
        }
    }

    /**
     * Set definitions
     */
    public function setDefinitions(array $definitions): void
    {
        $this->definitions = $definitions;
    }

    /**
     * Make DI container
     * @throws Exception
     */
    private function makeContainer(): Container
    {
        $builder = new ContainerBuilder();
        $builder->useAutowiring(false);
        $builder->useAnnotations(false);
        $builder->addDefinitions($this->definitions);

        return $builder->build();
    }

}