<?php

return [
    '/api/v1/auth' => [
        'method' => ['POST'], // Обработка только POST-запросов
        'action' => 'App\Controllers\AuthController@auth', // Какой метод контроллера будет запускаться
        'middleware' => [],
    ],
    '/api/v1/logout' => [
        'method' => ['GET'],
        'action' => 'App\Controllers\AuthController@logout',
        'middleware' => ['App\Middlewares\VerifyJWT'],
    ],
    '/api/v1/profile/{id:[0-9]+}' => [
        'method' => ['GET'],
        'action' => 'App\Controllers\ProfileController@index',
        'middleware' => ['App\Middlewares\VerifyJWT'],
    ],
    '/api/v1/postman/auth' => [
        'method' => ['POST'],
        'action' => 'App\Controllers\PostmanController@index',
        'middleware' => ['App\Middlewares\VerifyJWT'],
    ],
    '/api/v1/register' => [
        'method' => ['POST'],
        'action' => 'App\Controllers\RegisterController@index',
        'middleware' => [],
    ],
];
