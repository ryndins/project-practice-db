<?php

namespace App\Exceptions;

use Exception;

class NotValidUserDataException extends Exception
{
}