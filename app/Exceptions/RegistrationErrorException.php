<?php

namespace App\Exceptions;

use Exception;

class RegistrationErrorException extends Exception
{
}